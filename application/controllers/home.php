<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Home extends CI_Controller {


 function __construct()
 {
  //load relevant models
   parent::__construct();
   $this->load->model('user');
   $this->load->model('test_modules');
   $this->load->model('question_model');
 }

 function index()
 {
    //check  if user session exists
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $data['library_src'] = $this->jquery->script();
     $data['script_head'] = $this->jquery->_compile();
     $this->load->view('header', $data, FALSE);
     $data['std_info'] = $this->user->get_std_info($data['username']);

     //get test modules from the database to and plug them in to the user view.
     $data['test_modules'] = $this->test_modules->get_modules();
     $this->load->view('home_view', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('main', 'refresh');
   }
 }
    public function submit_ans(){
      // get user id
      $session_data = $this->session->userdata('logged_in');
      $id = $session_data['id'];
       $data = array();
       //explode form data to break into logical parts
      for($i=0; $i<count($_POST['ans']); $i++){
       
          $explode = explode('|', $_POST['ans'][$i]);
          $data[$i] = array(
                      'id'         => $id,
                      'question_id'=> $explode[1],
                      'answer'     => $explode[0]
                      );
      }
      // check if insert batch query runs.
      if($this->question_model->insert_answers($data)){
        
      }else{
        show_error('Something went wrong, Please try again!');
      }
  }

   function logout()
   {
      //unset user session data.
       $this->session->unset_userdata('logged_in');
       $this->session->sess_destroy();
       //redirect user to the login page
       redirect('main', 'refresh');
   }
   public function v_changepass(){
    $session_data = $this->session->userdata('logged_in');
     $data['username'] = $session_data['username'];
     $data['id'] = $session_data['id'];
      $this->load->view('header',$data);
      $this->load->view('changepass', $data);
   }
   public function changepass(){
    $id = $this->input->post('id');
    $pass = md5($this->input->post('pass'));
      if($this->user->changepass($id, $pass)){
        echo "Password successfully changed";
      }else{
        echo "Something went wrong.";
      }
   }
}

?>