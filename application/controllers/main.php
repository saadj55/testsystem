<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	function __construct()
	   {
	        parent::__construct();                   
	   }

	public function index()
	{
		$session_data = $this->session->userdata('logged_in');
		$data['username'] = $session_data['username'];
		$data['library_src'] = $this->jquery->script();
    	$data['script_head'] = $this->jquery->_compile();
		if(isset($session_data['username'])){
			redirect('home');
		}else{
			$this->load->view('header', $data);
			$this->load->view('splash');
			
		}
	}
}