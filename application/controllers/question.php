<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question extends CI_Controller {

	function __construct()
	 {
	   parent::__construct();
	   $this->load->model('question_model');
	 }

	public function index($questionid){
		$data['question'] = $this->question_model->get_questions($questionid);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data['question']));
	}
	public function get_questions_edit($select){
		$data['question'] = $this->question_model->get_questions_edit($select);
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data['question']));
	}
	function update_q(){
	 	$qid = $this->input->post('question_id');
	 	$question = $this->input->post('question');
	 	$opt1 = $this->input->post('opt1');
	 	$opt2 = $this->input->post('opt2');
	 	$opt3 = $this->input->post('opt3');
	 	$opt4 = $this->input->post('opt4');

	 	$this->question_model->update_question($qid, $question, $opt1, $opt2, $opt3, $opt4);
	 }
	 function add_q(){
	 	$question_id = $this->input->post('question_id');
	 	$question = $this->input->post('question');
	 	$opt1 = $this->input->post('opt1');
	 	$opt2 = $this->input->post('opt2');
	 	$opt3 = $this->input->post('opt3');
	 	$opt4 = $this->input->post('opt4');
	 	$module_id = $this->input->post('module_id');

	 	if($this->question_model->add_question($module_id, $question_id, $question, $opt1, $opt2, $opt3, $opt4)){
	 		echo "Question Added!";
	 	}else{
	 		echo "Something went wrong!";
	 	}
	 }
	 public function del_q($id){
	 	if($this->question_model->delete_question($id)){
	 		redirect('admin_home','refresh');
	 	}
	 }
	 public function get_last_row(){
		$data['question'] = $this->question_model->get_last_row();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data['question']));
	}
	public function search_ans(){
		$query = $this->input->post('query');
		$data = $this->question_model->search_ans($query);
		if($data){
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($data));
		}else{
			echo "No records found matching the given ID";
		}

	}
}