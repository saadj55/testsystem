<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_home extends CI_Controller {

	function __construct()
	 {
	   parent::__construct();
	   $this->load->model('test_modules');
	   $this->load->model('admin_model');
	   $this->load->model('question_model');
	 }

	function index()
 	{
	   if($this->session->userdata('admin_session'))
	   {
	     $session_data = $this->session->userdata('admin_session');
	     $data['username'] = $session_data['username'];
	     $this->load->view('header', $data, FALSE);
	     $data['test_modules'] = $this->test_modules->get_modules();
	     $this->load->view('add_question', $data);
	   }
	   else
	   {
	     redirect('admin', 'refresh');
	   }
    }
    public function get_modules(){
    	$data['modules'] = $this->test_modules->get_modules();
    	$this->output->set_content_type('application/json');
    	echo json_encode($data['modules']);
    }
    public function get_answers(){
    	$data['answers'] = $this->question_model->get_answers();
    	$this->output->set_content_type('application/json');
    	echo json_encode($data['answers']);
    }
    public function get_std(){
    	$data['std'] = $this->admin_model->get_std();
		$this->output->set_content_type('application/json');
		$this->output->set_output(json_encode($data['std']));
    }
    public function del_std($id){
    	if($this->admin_model->del_std(trim($id))){
    		redirect('admin_home','refresh');
    	}
    }
    public function update_std(){
    	$id = $this->input->post('id');
    	$username = $this->input->post('username');
    	$firstname = $this->input->post('firstname');
    	$lastname = $this->input->post('lastname');
    	$program = $this->input->post('program');

    	if($this->admin_model->update_std($id, $username, $firstname, $lastname, $program))
    	{
    		echo "Student Account Updated!";
    	}else{
    		echo "Something went wrong!";
    	}
    }
    public function create_id(){
    	$this->form_validation->set_rules('id', 'ID', 'trim|required|max_length[5]|xss_clean|is_unique[candidate.id]');
    	$this->form_validation->set_rules('firstname', 'First name', 'trim|required|xss_clean');
    	$this->form_validation->set_rules('lastname', 'Last name', 'trim|required|xss_clean');
    	$this->form_validation->set_rules('program', 'Program', 'trim|required|xss_clean');
    	$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|is_unique[candidate.username]');
    	$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('repassword', 'Password Confirmation', 'trim|required|matches[password]|callback_insert_intoDB');
		if($this->form_validation->run() == true)
	   	{
	     	//Validation is true.
	     	redirect('admin_home', 'refresh');
	   	}
	   	else
	   	{
	   		//Validation is false.
	   		$session_data = $this->session->userdata('logged_in');
	     	$data['username'] = $session_data['username'];
	   		$this->load->view('header' ,$data);
	     	$this->load->view('add_question');
	     	$this->load->view('footer');
	   	}
    }
    public function del_module($module_id){
    	$this->test_modules->del_module($module_id);
    	header('Location:'.base_url('admin_home'));
    }
    public function update_module(){
    	$module_id = $this->input->post('module_id');
    	$module_name = $this->input->post('module_name');
    	if($this->test_modules->update_module($module_id, $module_name)){
    		echo "Module Updated!";
    	}else{
    		echo "Something Went Wrong!";
    	}
    }
    public function add_module(){
    	$module_id = $this->input->post('module_id');
    	$module_name = $this->input->post('module_name');
    	if($this->test_modules->add_module($module_id, $module_name)){
    		echo "Module Added!";
    	}else{
    		echo "Something Went Wrong!";
    	}
    }
    public function insert_intoDB(){
    		$id = $this->input->post('id');
    		$username= $this->input->post('username');
	   		$password = md5($this->input->post('password'));
	   		$firstname = $this->input->post('firstname');
	   		$lastname = $this->input->post('lastname');
	   		$program = $this->input->post('program');
	     	if($this->admin_model->create_id_inDB($id, $username, $password, $firstname, $lastname, $program) == true){
	     		$this->form_validation->set_message('insert_intoDB', 'ID created Successfully!');
	     		return true;
	     	}else{
	     		$this->form_validation->set_message('insert_intoDB', 'Something went wrong!');
	     		return false;
	     	}
    }
	function logout()
	 {
	   $this->session->unset_userdata('logged_in');
	   $this->session->sess_destroy();
	   redirect('admin', 'refresh');
	 }
     public function v_changepass(){
      $session_data = $this->session->userdata('admin_session');
      $data['username'] = $session_data['username'];
      $data['id'] = $session_data['id'];
      $this->load->view('header',$data);
      $this->load->view('changepass_admin', $data);
   }
   public function changepass(){
    $username= $this->input->post('username');
    $pass = md5($this->input->post('pass'));
      if($this->admin_model->changepass($username, $pass)){
        echo "Password successfully changed";
      }else{
        echo "Something went wrong.";
      }
   }
}