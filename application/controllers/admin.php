<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
	    parent::__construct();                   
	}
	public function index(){
		//check if admin user exists.
		$session_data = $this->session->userdata('admin_session');
		$data['username'] = $session_data['username'];
		if(isset($session_data['username'])){
			$this->load->view('header', $data);
			$this->load->view('admin_home');
		}else{
			//if admin user doesn't exist redirect to admin login page.
			$this->load->view('header');
			$this->load->view('admin');		
		}
	}
}
?>