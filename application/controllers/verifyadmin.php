<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Verifyadmin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('admin_model');
	}

	function index(){

		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
		if($this->form_validation->run() == false)
	   	{
	     	//Field validation failed.  User redirected to login page
	     	$this->load->view('header');
        $this->load->view('admin');
	   	}
	   	else
	   	{
	     	//Go to the admin panel

	     	redirect('admin_home', 'refresh');
	   	}
	}

	function check_database(){
	//Field validation succeeded.  Validate against database
   	$username = $this->input->post('username');
   	$password = $this->input->post('password');
   //query the database
   $result = $this->admin_model->login($username, $password);

   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id'       =>$row->id,
         'username' =>$row->username
       );
       $this->session->set_userdata('admin_session', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Invalid username or password');
     return false;
   }
	}

}
?>