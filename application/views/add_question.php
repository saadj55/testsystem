<script type="text/javascript" src="<?echo base_url('assets/js/ajax_admin.js');?>" ></script>
<script type="text/javascript" src="<?echo base_url('assets/js/toastr.min.js');?>" ></script>
<link rel="stylesheet" type="text/css" href="<? echo base_url('assets/css/toastr.min.css');?>">
<div class="container">
	<div class="row">
	<legend>Create Student Account</legend>
	<div class="col-lg-6">
		<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
		<form action="<?echo base_url('admin_home/create_id');?>" method="post">
		<div class="form-group">
			    <label for="id">ID</label>
			    <input value="<? echo set_value('id');?>" type="text" class="form-control" name="id" id="id" placeholder="Enter ID">
			  </div>
			  <div class="form-group">
			    <label for="firstname">First Name</label>
			    <input type="text" value="<? echo set_value('firstname');?>" class="form-control" name="firstname" id="firstname" placeholder="Enter Student's First name">
			  </div>
			  <div class="form-group">
			    <label for="lastname">Last name</label>
			    <input type="text" class="form-control" value="<? echo set_value('lastname');?>" name="lastname" id="lastname" placeholder="Enter Student's Last name">
			  </div>
			  <div class="form-group">
			    <label for="program">Program</label>
			    <input type="text" class="form-control" value="<? echo set_value('program');?>" name="program" id="program" placeholder="Enter Student's Program">
			  </div>
			<div class="form-group">
			    <label for="username">Username</label>
			    <input type="text" class="form-control" value="<? echo set_value('username');?>" name="username" id="username" placeholder="Enter Username">
			  </div>
			  <div class="form-group">
			    <label for="password">Password</label>
			    <input type="text" name="password" value="<? echo set_value('password');?>" class="form-control" id="password" placeholder="Enter Password">
			  </div>
			  <div class="form-group">
			    <label for="retype-password">Confirm Password</label>
			    <input type="text" name="repassword" class="form-control" value="<? echo set_value('repassword');?>" id="retype-password" placeholder="Retype Password">
			  </div>
			  <div class="form-group">
			  	<input type="submit" class="btn btn-primary" value="Submit">
			  </div>
		<? echo form_close();?>
	</div></div>
	<div class="row">
	<legend>Manage Accounts</legend>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Username</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Program</th>
					<th>Edit/Delete</th>
				</tr>
			</thead>
			<tbody id="tb">
				<tr></tr>
			</tbody>
		</table></div></div>
	<div class="row">
	<legend>Manage Modules</legend>
	<!-- Button trigger modal -->
<button class="btn btn-primary" data-toggle="modal" data-target="#myModal">
  <span class="glyphicon glyphicon-plus"></span> Add Module
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Module</h4>
      </div>
      <div class="modal-body">
        
        	<label>Module ID</label>
        	<input class="form-control" type="text" id="module-id-add" required><br>
        	<label>Module Name</label>
        	<input class="form-control" type="text" id="module-name-add" required>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
        <button onclick="btn_add_module()" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add</button>
      </div>
    </div>
  </div>
</div>
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Module ID</th>
					<th>Module</th>
					<th>Edit/Delete</th>
				</tr>
			</thead>
			<tbody id="m_modules"></tbody>
		</table>
		</div>
		</div>
	<div class="row">
	<legend>Manage Questions</legend>
	<div class="col-lg-6">
		<select id="test_module" class="form-control">
			<option value="">Please select an option...</option>
		<? foreach ($test_modules as $row){ ?>
			<option value="<? echo $row->module_id; ?>"><? echo $row->module_name; ?></option>
		<? } ?>
		</select>
	</div><!-- Button trigger modal -->
	<button id="add_a_question"class="btn btn-primary" data-toggle="modal" data-target="#modal_question">
	  <span class="glyphicon glyphicon-plus"></span> Add Questions
	</button>

<!-- Modal -->
<div class="modal fade" id="modal_question" tabindex="-1" role="dialog" aria-labelledby="add_a_question" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="add_a_question">Add Questions</h4>
      </div>
      <div class="modal-body" id="modal-body-question">
      		<label>Select Module</label>
            <select id="question-add-sel-mod" class="form-control">
				<option value="">Please select an option...</option>
				<? foreach ($test_modules as $row){ ?>
					<option value="<? echo $row->module_id; ?>"><? echo $row->module_name; ?></option>
				<? } ?>
			</select><br>
			<label>Question ID</label>
			<input type="text" class="form-control" id="question-id-add"><br>
			<label>Question</label>
			<input type="text" class="form-control" id="question-name-add"><br>
			<label>Option 1</label>
			<input type="text" class="form-control" id="question-opt1-add"><br>
			<label>Option 2</label>
			<input type="text" class="form-control" id="question-opt2-add"><br>
			<label>Option 3</label>
			<input type="txt" class="form-control" id="question-opt3-add"><br>
			<label>Option 4</label>
			<input type="text" class="form-control" id="question-opt4-add"><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
        <button onclick="btn_add_question()" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add</button>
      </div>
    </div>
  </div>
</div>
	<div class="table-responsive">
		<table class="table table-hover" id="q_tb">
			<thead>
				<tr>
					<th>Question</th>
					<th>Option 1</th>
					<th>Option 2</th>
					<th>Option 3</th>
					<th>Option 4</th>
					<th>Edit/Delete</th>
				</tr>
			</thead>
			<tbody id="qtb">
			</tbody>
		</table></div></div>
	<div class="row">
		<legend>Submitted Answers</legend>
		<div class="col-lg-4">
		<label>Search By ID:</label>
		<form id="search-form">
			<input class="form-control" type="text" id="search-ans">
			<button class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Search</button>
		<form>
		</div>
		<div class="table-responsive">
			<table class="table table-hover" id="tb-head-ans">
				<thead>
					<tr>
						<th>ID</th>
						<th>Question ID</th>
						<th>Answer</th>
						<th>Date & Time</th>
					</tr>
				</thead>
				<tbody id="tb-ans"></tbody>
			</table>
		</div>
	</div>
