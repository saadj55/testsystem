<div class="container-fluid">
<div class="col-lg-4 col-lg-offset-4">
<script type="text/javascript" src="<?echo base_url('assets/js/ajax_student.js');?>" ></script>
<script type="text/javascript" src="<?echo base_url('assets/js/toastr.min.js');?>" ></script>
<link rel="stylesheet" type="text/css" href="<? echo base_url('assets/css/toastr.min.css');?>">
<input type="hidden" id="id" value="<?echo $id;?>">
<?php if(isset($id)){?>
<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
		        <form>
				  <div class="form-group">
				    <label for="exampleInputEmail1">Current Password</label>
				    <input id="currentpass" type="password" class="form-control" placeholder="Enter Current Password">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">New Password</label>
				    <input id="newpass" type="password" class="form-control"  placeholder="Enter New Password">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Confirm New Password</label>
				    <input id="cpass" type="password" class="form-control"  placeholder="Confirm New Password">
				  </div>
				  <button type="button" id="btn-cpass"class="btn btn-primary">Change Password</button>
				  <script type="text/javascript">
				  	$('#btn-cpass').on('click', function(){
						var id = $('#id').val();
						var pass = $('#newpass').val().trim();
						var cpass = $('#cpass').val().trim();
						var cupass = $('#currentpass').val().trim();
					if( cupass=="" && pass=="" && cpass==""){
						toastr.options.closeButton = true;
						toastr.info('All fields are required.','Info');
					}else if(pass !== cpass){
						toastr.options.closeButton = true;
						toastr.error('Passwords do no match!','Error');	
					}else{
						var data = {
							id: id,
							pass: cpass
						};
						$.ajax({
								url: 'changepass',
								method: 'POST',
								data: data,
								success: function(msg){
										toastr.options.closeButton = true;
										toastr.success(msg,'Success');	
								}
							});
					}			
				});
				  </script>
				<?echo form_close();?></div></div>
				<?php }else{
					show_error('Please Login to continue.');
				}?>
