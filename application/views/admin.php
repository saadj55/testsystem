<div class="container">
	<div class="jumbotron">
		<h2 style="font-family: 'Raleway', sans-serif;">Welcome to IoBM Test System Admin Panel</h2>
				<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
		        <?echo form_open('verifyadmin');?>
				  <div class="form-group">
				    <label for="exampleInputEmail1">Username</label>
				    <input name="username" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
				  </div>
				  <div class="form-group">
				    <label for="exampleInputPassword1">Password</label>
				    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Enter Password">
				  </div>
				  <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-log-in"></span>&nbsp;Login</button>
				<?echo form_close();?>
	</div>