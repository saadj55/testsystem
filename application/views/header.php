<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
		<title><?
      $user = $this->session->userdata('logged_in');
      $admin = $this->session->userdata('admin_session');
      if(isset($user['username']) && !isset($admin['username'])){
        echo $user['username']." - ITS";
      }elseif(isset($admin['username']) && !isset($user['username'])){
        echo $admin['username']." - ITS";
      }else{
        echo "ITS";
      }
      ?>
    </title>
    <style type="text/css">
      .animate{
        -webkit-transition:all 2s;
      }
    </style>
		<link rel="stylesheet" type="text/css" href="<?echo base_url('assets/css/bootstrap.css');?>">
    <link rel="stylesheet" type="text/css" href="<?echo base_url('assets/js/timer/CSS/timer.css');?>">
		<script type="text/javascript" src="<?echo base_url('assets/js/jquery.js');?>"></script>
		<script type="text/javascript" src="<?echo base_url('assets/js/bootstrap.js');?>"></script>
    <script type="text/javascript" src="<?echo base_url('assets/js/timer/timer.js');?>"></script>
    <link href='http://fonts.googleapis.com/css?family=Raleway:300' rel='stylesheet' type='text/css'>
	</head>
	<body>
  <nav class="navbar navbar-inverse" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" style="font-family: 'Raleway', sans-serif;" href="<? echo base_url();?>">IoBM Test System</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <?if (isset($user['username']) && !isset($admin['username'])){?>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>&nbsp;<? echo $user['username'];?><b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="<?echo base_url('home/logout');?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
            <li><a href="<? echo base_url('home/v_changepass');?>"><span class="glyphicon glyphicon-wrench"></span> Change Password</a></li>
          </ul>
        </li>
      </ul>
      <?}elseif(isset($admin['username']) && !isset($user['username'])){?>
          <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>&nbsp;<? echo $admin['username'];?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?echo base_url('admin_home/logout');?>"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a></li>
              <li><a href="<? echo base_url('admin_home/v_changepass');?>"><span class="glyphicon glyphicon-wrench"></span> Change Password</a></li>
            </ul>
          </li>
        </ul>
      <?}else{
        echo "";
        }?>
    </div>
  </div>
</nav>
