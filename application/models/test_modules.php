<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_modules extends CI_Model {

	public function get_modules(){
		$this -> db -> select('*');
		$this -> db -> from('test_modules');
		$query = $this->db->get();
		return $query->result();
	}
	public function update_module($module_id, $module_name){
		$sql = "UPDATE test_modules SET module_name='$module_name' WHERE module_id='$module_id'";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}
	public function del_module($module_id){
		$this->db->delete('test_modules', array('module_id' => $module_id)); 
	}
	public function add_module($module_id, $module_name){
		$sql = "INSERT INTO test_modules(module_id, module_name) VALUES('$module_id', '$module_name')";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}
	}
}
?>