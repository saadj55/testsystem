<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends CI_Model {

	public function get_questions($module_id){
		$this->db->select('*');
		$this->db->from('questions');
		$this->db->where('module_id', $module_id);

		return $this->db->get()->result();
	}
	public function get_questions_edit($select){
		$this->db->select('*');
		$this->db->from('questions');
		$this->db->where('module_id', $select);
		return $this->db->get()->result();
	}

	public function update_question($qid, $question, $opt1, $opt2, $opt3, $opt4){
		$sql = "UPDATE questions SET question='$question', opt1='$opt1', opt2='$opt2', opt3='$opt3', opt4='$opt4' WHERE question_id='$qid'";
		if($this->db->query($sql)){
			return true;
		}
	}
	public function add_question($module_id, $qid, $question, $opt1, $opt2, $opt3, $opt4){
		$sql1 = "INSERT INTO questions(module_id, question_id, question, opt1, opt2, opt3, opt4) VALUES ('$module_id', '$qid','$question', '$opt1', '$opt2', '$opt3', '$opt4')";
		if($this->db->query($sql1)){
			return true;
		}
	}
	public function delete_question($question_id){
		$sql = "DELETE FROM questions WHERE question_id='$question_id'";
		if($this->db->query($sql)){
			return true;
		}
	}
	public function get_last_row(){
		$sql = "SELECT * FROM questions";
		$query = $this->db->query($sql);
		return $query->last_row();
	}
	public function insert_answers($data){
		$sql = $this->db->insert_batch('answers', $data);
		if($sql){
			return true;
		}else{
			return false;
		}
	}
	public function get_answers(){
		$sql = "SELECT id, question_id, answer, dt FROM answers";
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function get_questions_tooltip(){
		$this->db->select('answers.question_id, questions.question');
		$this->db->from('answers');
		$this->db->join('questions', 'answers.question_id = questions.question_id', 'inner');

		$query = $this->db->get();
		return $query->result();
	}

	public function search_ans($query){
		$sql = "SELECT * FROM answers WHERE id LIKE '$query%'";
		$query = $this->db->query($sql);
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}
}
?>