<? if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function login($username, $password){
		   $this -> db -> select('*');
		   $this -> db -> from('admin');
		   $this -> db -> where('username', $username);
		   $this -> db -> where('password', MD5($password));
		   $this -> db -> limit(1);

		   $query = $this -> db -> get();

		   if($query -> num_rows() == 1)
		   {
		     return $query->result();
		   }
		   else
		   {
		     return false;
		   }
	}
	public function get_std(){
		$this->db->select('candidate.id, candidate.username, std_info.firstname, std_info.lastname, std_info.program');
		$this->db->from('candidate');
		$this->db->join('std_info', 'candidate.id = std_info.id', 'inner');

		$query = $this->db->get();
		return $query->result();
	}
	function del_std($id){
		$sql1 = "DELETE FROM std_info WHERE id=$id";
		$sql2 = "DELETE FROM candidate WHERE id=$id";
		$q1 = $this->db->query($sql1);
		$q2 = $this->db->query($sql2);

		if($q1 && $q2){
			return true;
		}else{
			return false;
		}
	}
	public function changepass($username, $pass){
		$sql = "UPDATE admin SET password = '$pass' WHERE username='$username'";
		if($this->db->query($sql)){
			return true;
		}else{
			return false;
		}

	}

	public function create_id_inDB($id, $user, $pass, $firstname, $lastname, $program){
		$sql1 = "INSERT INTO candidate(id, username, password) VALUES ($id, '$user', '$pass')";
		$sql2 = "INSERT INTO std_info(id, firstname, lastname, program) VALUES ('$id', '$firstname', '$lastname', '$program')";
		if($this->db->query($sql1) && $this->db->query($sql2)){
			return true;
		}
	}
	public function update_std($id, $username, $firstname, $lastname, $program){
		$sql1 = "UPDATE std_info SET firstname='$firstname', lastname='$lastname', program='$program' WHERE id=$id";
		$sql2 = "UPDATE candidate SET username='$username' WHERE id=$id";

		if($this->db->query($sql1) && $this->db->query($sql2)){
			return true;
		}
	}
}
?>