	$(document).ready(function(){
		$("#test_module").change(function(e){
			$('#datax > form').empty();
			var select = $('#test_module').val();
			var url = "question/index/"+select;	
			var qno = 1;
			var i = 0;
			if(select == ""){
				$('#datax').hide();
			}else{
				$.getJSON(url, function(json){

					$('#datax').show();
					for(var d=0; d<json.length; d++){
							$("#datax > form").append("<span>"+qno+".<strong>"+json[d].question+"</strong></span><br>");
							$("#datax > form").append('<div class="radio"><label><input type="radio" name="ans['+i+']" value="'+json[d].opt1+"|"+json[d].question_id+"|"+json[d].module_id+'"> '+json[d].opt1+'</label></div>');
							$("#datax > form").append('<div class="radio"><label><input type="radio" name="ans['+i+']" value="'+json[d].opt2+"|"+json[d].question_id+"|"+json[d].module_id+'"> '+json[d].opt2+'</label></div>');
							$("#datax > form").append('<div class="radio"><label><input type="radio" name="ans['+i+']" value="'+json[d].opt3+"|"+json[d].question_id+"|"+json[d].module_id+'"> '+json[d].opt3+'</label></div>');
							$("#datax > form").append('<div class="radio"><label><input type="radio" name="ans['+i+']" value="'+json[d].opt4+"|"+json[d].question_id+"|"+json[d].module_id+'"> '+json[d].opt4+'</label></div>');
							$("#datax > form").append('<hr>');
							qno++;
							i++;

						}
						$('#datax > form').append('<div class="col-lg-6 col-lg-offset-3"><input id="btn-submit-ans" type="button" class="btn btn-primary btn-lg btn-block" value="Submit"></div>');	
				});
			}
			e.preventDefault();	
		});
		
	});
	$(document).on('click', '#btn-submit-ans', function(){
		var data = $('#form-questions').serialize();
        $.ajax({
        	url: 'home/submit_ans',
        	method: 'POST',
        	data: data,
        	success: function(msg){
					toastr.options.closeButton = true;
					toastr.success("Submission Complete!",'Success');
        		}
        	});
      });

