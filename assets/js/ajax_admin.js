var sel = "";

$(document).ready(function(){
	var url= "admin_home/get_std/";
	$.getJSON(url, function(json){
		for(var d=0; d<json.length; d++){
				id = json[d].id;
				username = json[d].username;
				firstname = json[d].firstname;
				lastname = json[d].lastname;
				program = json[d].program;
				var html = "<tr id="+id+"><td>"+id+"</td>"+
						   "<td>"+username+"</td>"+
						   "<td>"+firstname+"</td>"+
						   "<td>"+lastname+"</td>"+
						   "<td>"+program+"</td>"+
						   "<td><button id='btn"+id+"' onclick='edit_std(\""+id+"\",\""+username+"\",\""+firstname+"\",\""+lastname+"\",\""+program+"\")' class='btn btn-primary'><span class='glyphicon glyphicon-edit'></span>&nbsp;Edit</button>&nbsp;"+
						   "<a href='admin_home/del_std/"+id+"' class='btn btn-danger'><span class='glyphicon glyphicon-trash'></span>&nbsp;Delete</a></td></tr>";
				$("#tb").append(html);		
			}
	});
	$.getJSON('admin_home/get_modules/', function(json){
		for(i=0; i<json.length; i++){
			var html = "<tr id="+json[i].module_id+"><td>"+json[i].module_id+"</td>"+
						"<td>"+json[i].module_name+"</td>"+
						"<td><button id='btn-"+json[i].module_id+"' onclick='edit_module(\""+json[i].module_id+"\")' class='btn btn-primary'><span class='glyphicon glyphicon-edit'></span> Edit</button>&nbsp;<a href='admin_home/del_module/"+json[i].module_id+"' class='btn btn-danger'><span class='glyphicon glyphicon-trash'></span> Delete</a></td></tr>";
				$('#m_modules').append(html);
		}
	});
		
			$.getJSON('admin_home/get_answers/', function(json){
			for(i=0; i<json.length; i++){
				html = "<tr><td>"+json[i].id+"</td>"+
						"<td>"+json[i].question_id+"</td>"+
						"<td>"+json[i].answer+"</td>"+
						"<td>"+json[i].dt+"</td></tr>";
						$('#tb-ans').append(html);
			}
		});
		$('#search-form').submit(function(event){
			var query = $('#search-ans').val().trim();
			if(query == ""){
				toastr.options.closeButton = true;
				toastr.info('Please enter any Student ID','Info');
			}else{
				$.ajax({
					url:'question/search_ans',
					method:'POST',
					data: {query: query},
					success: function(json){
						if(typeof json == 'string'){
							toastr.options.closeButton = true;
							toastr.error('No answers found matching the given ID.','Error');
						}else{
							$('#tb-ans').empty();
							for(i=0; i<json.length; i++){
								html = "<tr><td>"+json[i].id+"</td>"+
								"<td>"+json[i].question_id+"</td>"+
								"<td>"+json[i].answer+"</td>"+
								"<td>"+json[i].dt+"</td></tr>";
								$('#tb-ans').append(html);
							}
						}
					}
				});
			}
			event.preventDefault();
		});
	
});
function edit_module(module_id){
		var moduleid = '<input id="moduleid_edit" type="hidden" value='+module_id+'>';
		var modulename = '<input class="form-control" id="modulename_edit" size="6" type="text">';
		$('#'+module_id+' > td:nth-child(2)').html(modulename);
		$('#'+module_id+' > td:nth-child(1)').append(moduleid);
		$('#btn-'+module_id).html('Update');
		$('#btn-'+module_id).attr('onclick','update_module()');
}
function update_module(){
	url = "admin_home/update_module";
	var data={
		module_name: $('#modulename_edit').val(),
		module_id: $('#moduleid_edit').val()
	};
	$.ajax({
		url: url,
		data: data,
		method:'POST',
		success: function(msg){
			toastr.options.closeButton = true;
			toastr.success(msg,'Success');
			refresh_module();
		}
	});
}
function refresh_module(){
	$('#m_modules').empty();
	$.getJSON('admin_home/get_modules/', function(json){
		for(i=0; i<json.length; i++){
			var html = "<tr id="+json[i].module_id+"><td>"+json[i].module_id+"</td>"+
						"<td>"+json[i].module_name+"</td>"+
						"<td><button id='btn-"+json[i].module_id+"' onclick='edit_module("+json[i].module_id+")' class='btn btn-primary'><span class='glyphicon glyphicon-edit'></span> Edit</button>&nbsp;<a href='admin_home/del_module/"+json[i].module_id+"' class='btn btn-danger'><span class='glyphicon glyphicon-trash'></span> Delete</a></td></tr>";
				$('#m_modules').append(html);
		}
	});
}

function btn_add_module(){
	id = $('#module-id-add').val();
	name = $('#module-id-add').val();
	var data = {
			module_id: id,
			module_name: name
		};
	if(id == "" && name == ""){
		var err = $('<div class="alert alert-danger">All fields are required.</div>').fadeIn(400).delay(4000).fadeOut(400);
		$('.modal-body').append(err);
	}else{
		$.ajax({
			url    : 'admin_home/add_module',
			method : 'POST',
			data   : data,
			success: function(msg){
				var error = $('<div class="alert alert-info">'+msg+'</div>').fadeIn(400).delay(4000).fadeOut(400);
				$('.modal-body').append(error);
			}
		});
	}
}
function std_refresh(){
	var url= "admin_home/get_std/";
	$('#tb').empty();
	$.getJSON(url, function(json){
		for(var d=0; d<json.length; d++){
				id = json[d].id;
				username = json[d].username;
				firstname = json[d].firstname;
				lastname = json[d].lastname;
				program = json[d].program;
				var html = "<tr id="+id+"><td>"+id+"</td>"+
						   "<td>"+username+"</td>"+
						   "<td>"+firstname+"</td>"+
						   "<td>"+lastname+"</td>"+
						   "<td>"+program+"</td>"+
						   "<td><input id='btn"+id+"' type='button' value='Edit' onclick='edit_std(\""+id+"\",\""+username+"\",\""+firstname+"\",\""+lastname+"\",\""+program+"\")' class='btn btn-primary'>&nbsp;"+
						   "<a href='admin_home/del_std/"+id+"' class='btn btn-danger'><span class='glyphicon glyphicon-trash'></span>Delete</a></td></tr>";
						   
						$("#tb").append(html);		
			}
	});
}

function edit_std(id,username, firstname, lastname, program){
		
	var ids = '<input id="id_edit" size="5" type="hidden" value="'+id+'"/>';
	var usernames = '<input class="form-control" id="username_edit" size="6" type="text" value="'+username+'">';
	var firstnames = '<input class="form-control" id="firstname_edit" size="6" type="text" value="'+firstname+'">';
	var lastnames = '<input class="form-control" id="lastname_edit" size="6" type="text" value="'+lastname+'">';
	var programs = '<input class="form-control" id="program_edit" size="6" type="text" value="'+program+'">';
	$('#'+id+' > td:nth-child(1)').append(ids);
	$('#'+id+' > td:nth-child(2)').html(usernames);
	$('#'+id+' > td:nth-child(3)').html(firstnames);
	$('#'+id+' > td:nth-child(4)').html(lastnames);
	$('#'+id+' > td:nth-child(5)').html(programs);
	$('#btn'+id).val('Update');
	$('#btn'+id).attr('onclick','update_std()');
}

function update_std(){
	var url = "admin_home/update_std/";
	var postData = 
	{
			'id'       : $('#id_edit').val(),
			'username' : $('#username_edit').val(),
			'firstname': $('#firstname_edit').val(),
			'lastname' : $('#lastname_edit').val(),
			'program'  : $('#program_edit').val()
	};
	$.ajax({
		type: "POST",
		url: url,
		data: postData,
		success: function(msg){
			toastr.options.closeButton = true;
			toastr.success(msg,'Success');
			std_refresh();
		}
	});

}

$(document).ready(function(){
		$("#test_module").change(function(e){
			sel = $('#test_module').val();
			var select = $('#test_module').val();
			var url = "question/get_questions_edit/"+select;
			if(select == ""){
				//$('#q_tb').hide();
			}else{
				$('#qtb > tr').remove();
				$.getJSON(url, function(json){
					for(var d=0; d<json.length; d++){
						var qid = json[d].question_id;
						var question = json[d].question;
						var opt1 = json[d].opt1;
						var opt2 = json[d].opt2;
						var opt3 = json[d].opt3;
						var opt4 = json[d].opt4;
						var html = "<tr id="+qid+"><td>"+question+"</td>"+
							   "<td>"+opt1+"</td>"+
							   "<td>"+opt2+"</td>"+
							   "<td>"+opt3+"</td>"+
							   "<td>"+opt4+"</td>"+
							   "<td><input id='btn"+qid+"' type='button' value='Edit' onclick='edit_q(\""+qid+"\",\""+question+"\",\""+opt1+"\",\""+opt2+"\",\""+opt3+"\",\""+opt4+"\")' class='btn btn-primary'>&nbsp;"+
							   "<a href='question/del_q/"+qid+"' class='btn btn-danger'><span class='glyphicon glyphicon-trash'></span> Delete</a></td></tr>";
							   $("#qtb").append(html);
						}	
				});
			}
			e.preventDefault();	
		});
		
	});
function refresh_q(){
	sel = $('#test_module').val();
			var select = $('#test_module').val();
			var url = "question/get_questions_edit/"+select;
			if(select == ""){
				//$('#q_tb').hide();
			}else{
				$('#qtb > tr').remove();
				$.getJSON(url, function(json){
					for(var d=0; d<json.length; d++){
						var qid = json[d].question_id;
						var question = json[d].question;
						var opt1 = json[d].opt1;
						var opt2 = json[d].opt2;
						var opt3 = json[d].opt3;
						var opt4 = json[d].opt4;
						var html = "<tr id="+qid+"><td>"+question+"</td>"+
							   "<td>"+opt1+"</td>"+
							   "<td>"+opt2+"</td>"+
							   "<td>"+opt3+"</td>"+
							   "<td>"+opt4+"</td>"+
							   "<td><input id='btn"+qid+"' type='button' value='Edit' onclick='edit_q(\""+qid+"\",\""+question+"\",\""+opt1+"\",\""+opt2+"\",\""+opt3+"\",\""+opt4+"\")' class='btn btn-primary'>&nbsp;"+
							   "<a href='question/del_q/"+qid+"' class='btn btn-danger'><span class='glyphicon glyphicon-trash'></span> Delete</a></td></tr>";
							   $("#qtb").append(html);
						}	
				});
			}
}
function edit_q(qid, question, opt1, opt2, opt3, opt4){
	var qids = '<input class="form-control" id="qid_edit" size="5" type="hidden" value="'+qid+'"/>';
	var questions = '<textarea rows="5" class="form-control" id="question_edit">"'+question+'"</textarea>';
	var opt1s = '<input id="opt1_edit" size="10" class="form-control" type="text" value="'+opt1+'">';
	var opt2s = '<input id="opt2_edit" size="10" class="form-control" type="text" value="'+opt2+'">';
	var opt3s = '<input id="opt3_edit" size="10" class="form-control" type="text" value="'+opt3+'">';
	var opt4s = '<input id="opt4_edit" size="10" class="form-control" type="text" value="'+opt4+'">';
	$('#'+qid+' > td:nth-child(1)').html(questions);
	$('#'+qid+' > td:nth-child(2)').html(opt1s);
	$('#'+qid+' > td:nth-child(3)').html(opt2s);
	$('#'+qid+' > td:nth-child(4)').html(opt3s);
	$('#'+qid+' > td:nth-child(5)').html(opt4s);
	$('#'+qid+' > td:nth-child(6)').append(qids);
	$('#btn'+qid).val('Update');
	$('#btn'+qid).attr('onclick','update_q()');
}
function update_q(){
	var url = "question/update_q/";
	var Data = 
					{
							'question_id': $('#qid_edit').val(),
							'question'   : $('#question_edit').val(),
							'opt1'       : $('#opt1_edit').val(),
							'opt2'   	 : $('#opt2_edit').val(),
							'opt3'    	 : $('#opt3_edit').val(),
							'opt4'       : $('#opt4_edit').val()
					};
	if(Data.question_id=="" && Data.question=="" && Data.opt1=="" && Data.opt2=="" && Data.opt3=="" && Data.opt4==""){
		toastr.options.positionClass = "toast-top-right";
		toastr.options.closeButton = true;
		toastr.info('Something went wrong!');
	}else{
		$.ajax({
			type: "POST",
			url: url,
			data: Data,
			success: function(msg){
					toastr.options.positionClass = "toast-top-right";
					toastr.options.closeButton = true;
					toastr.info('Question Updated!');
					refresh_q();
			}
		});
	}

}
function remove(){
	$('#qtb > tr:nth-last-child(1)').remove();
}
function btn_add_question(){
	sel = $('#question-add-sel-mod').val();
	qid = $('#question-id-add').val()
	question = $('#question-name-add').val();
	opt1 = $('#question-opt1-add').val();
	opt2 = $('#question-opt2-add').val();
	opt3 = $('#question-opt3-add').val();
	opt4 = $('#question-opt4-add').val();
	var postdata = {
		module_id: sel,
		question_id:qid ,
		question : question,
		opt1     : opt1,
		opt2     : opt2,
		opt3     : opt3,
		opt4     : opt4
	};
	if(sel == "" && qid == "" && question=="" && opt1=="" && opt2=="" && opt3=="" && opt4==""){
		var error = $('<div class="alert alert-danger">All fields are required</div>').fadeIn(400).delay(4000).fadeOut(400);
		$('#modal-body-question').append(error);
	}else{
		$.ajax({
			url    : 'question/add_q/',
			method : 'POST',
			data   : postdata,
			success: function(msg){
				var err = $('<div class="alert alert-info">'+msg+'</div>').fadeIn(1000).delay(4000).fadeOut(400);
				$('#modal-body-question').append(err)
				refresh_q();
			}
		});
	}
}
